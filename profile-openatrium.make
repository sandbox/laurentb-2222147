;******************************************************************************
;                                Documentation
;******************************************************************************

; Description:
; A drush makefile for OpenAtrium profile.

; Instructions:
; Include this makefile in a stub makefile like so:
;   includes[openatrium] = "http://drupalcode.org/project/planeo_makefiles.git/blob_plain/fa7ae8edb3d7c26691657178fb69879c77b1eb15:/profile-openatrium.make"
; This makefile checks out the latest commit to git of Open Atrium. If you'd
; like to point to a specific release version add:
;   projects[openatrium][download][tag] = "6.x-1.2"

; /////////////////////////////// REMINDER ////////////////////////////////////
; Update stub makefiles after any commits altering this makefile, so as to 
; refer to the latest commit hash.

;******************************************************************************
;                                   General
;******************************************************************************

; drush make API version
api = 2

; Drupal core
core = 6.x
;projects[drupal][version] = 6.30

;******************************************************************************
;                              Open Atrium profile
;******************************************************************************

projects[openatrium][type] = profile
projects[openatrium][download][type] = git
projects[openatrium][download][url] = git://drupalcode.org/project/openatrium.git




;******************************************************************************
;                                   Patches
;******************************************************************************

projects[openatrium][patch][] = http://drupalcode.org/sandbox/laurentb/2220309.git/blob_plain/35a97ae0e5f25ed0e8d4882120739ce1eaf7f50d:/[openatrium]-[profil]-[1]-[1].patch


;projects[context][subdir] = "contrib" 
;projects[context][version] = "3.3"

;projects[ctools][subdir] = "contrib"
;projects[ctools][version] = "1.11"

;projects[nodeformcols][subdir] = "contrib"
;projects[nodeformcols][version] = "1.7"

;projects[markdown][subdir] = "contrib"
;projects[markdown][version] = "1.4"

projects[views][subdir] = "contrib"
projects[views][version] = "2.16"
projects[views][patch][] = "https://drupal.org/files/views-1432242-5-fix-size-notice.patch"
pojects[views][patch][] = "https://drupal.org/files/views-465332-11-d6-2x.patch"
projects[views][patch][] = "https://drupal.org/files/views-465332-30.patch"


;******************************************************************************
;                                     End
;******************************************************************************
