;******************************************************************************
;                               Documentation
;*i****************************************************************************

; Description:
; A drush makefile for Drupal core.

; Instructions:
; This makefile checks out the latest commit to git of Drupal 6.x. If you'd
; like to point to a specific release version, include this makefile in a stub,
; and add:
;   e.g., projects[drupal][download][tag] = "6.25"

; /////////////////////////////// REMINDER ////////////////////////////////////
; Update stub makefiles after any commits altering this makefile, so as to
; refer to the latest commit hash.

;******************************************************************************
;                                  General
;******************************************************************************

; drush make API version
api = 2

; Drupal core
core = 6.x
projects[drupal][version] = 6.30

;******************************************************************************
;                                 Drupal core
;******************************************************************************

projects[drupal][type] = core
;projects[drupal][download][type] = git
;projects[drupal][download][url] = "http://git.drupal.org/project/drupal.git"


;******************************************************************************
;                                  Patches
;******************************************************************************

; none

;******************************************************************************
;                                    End
;******************************************************************************
