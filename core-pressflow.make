;******************************************************************************
;                               Documentation
;*i****************************************************************************

; Description:
; A drush makefile for Pressflow core.

; Instructions: 
; This makefile checks out the latest commit to git of Pressflow. If you'd like
; to point to a specific release version, include this makefile in a stub, and
; add:
;   e.g., projects[pressflow][download][tag] = "6.25"

; /////////////////////////////// REMINDER ////////////////////////////////////
; Update stub makefiles after any commites altering this makefile, so as to
; refer to the latest commit hash.

;******************************************************************************
;                                  General
;******************************************************************************

; drush make API version
api = 2

; Drupal core
core = 6.x
;projects[drupal][version] = 6.30

;******************************************************************************
;                               Pressflow core
;******************************************************************************

projects[pressflow][type] = core
projects[pressflow][download][type] = git
projects[pressflow][download][url] = git://github.com/omega8cc/pressflow6.git



;******************************************************************************
;                                  Patches
;******************************************************************************

; none

;******************************************************************************
;                                    End
;******************************************************************************
