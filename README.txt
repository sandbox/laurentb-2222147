
planeo.com makefiles
=======================
Makefiles used to maintain the platforms for planeo.com

Use with the Drush Make command. stub-planeo.make is the most useful
makefile. All with all stub makefiles, you'll need the other parts to make it
work.

Usage
-----
If it's not already done, you need to clone the whole repository. Got to your
makefile folder and run 

git clone --branch master http://git.drupal.org/project/planeo_makefiles.git

Or download the latest version from 
http://drupal.org/project/planeo_makefiles

Then go where you want the code base to be created and run
drush make yourmakefilefolder/planeo_makefiles/stub-planeo.make

Alternatively, you can use these makefiles to create a new Aegir platform.
Just point to stub-planeo.make in the platform creation interface.

Maintainers
-----------
These makefiles are maintained and distributed by lolostudio for the purpose of
facilitating the creation of Open Atrium platforms.

See http://drupal.org/project/planeo_makefiles for more information.
