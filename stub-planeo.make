;******************************************************************************
;                                Documentation
;******************************************************************************
; Make OpenAtria.com makefile
; http://drupal.org/project/planeo_makefiles 
;
; Description:
; A stub drush makefile to create an up-to-date planeo installation profile
; on a Pressflow core. The standard Open Atrium platform is extended with
; additional features for OpenAtria.com, and some modules will be overridden in
; sites/all (patches, dev/git versions, &c.)
;
; Instructions:
; In your platforms directory (/var/aegir/platforms), run:
;   "drush make openatria-stub.make"
; Or create a new platform by calling this makefile using Aegir's web interface
; ('Makefile' field on node/add/platform):
;   "/var/aegir/makefiles/planeo_makefiles/stub-planeo.make"
; Better yet, point to the raw (blob_plain) display of latest git commit
; remotely, so you can keep an exact reference in Aegir:
;   e.g., http://drupalcode.org/project/planeo_makefiles.git/blob_plain/<most recent commit hash>:/stub-planeo.make

;******************************************************************************
;                                   General
;******************************************************************************

; drush make API version
api = 2

; Drupal core
core = 6.x
;projects[drupal][version] = 6.30

;******************************************************************************
;                               Include files
;******************************************************************************

; Since we're including another stub makefile, we already get core and
; openatrium.profile
includes[drupal] = "stub-openatrium.make"

;******************************************************************************
;                            Patches / Overrides
;******************************************************************************



;******************************************************************************
;                            Additional features
;******************************************************************************
; N.B.: makefiles recursively included in these features will *not* override
; those in the profile or other previously run makefile. So when we need to
; override, we include them here.


projects[user_limit][version]= "1.x-dev"
;git clone --branch 6.x-1.x http://git.drupal.org/project/user_limit.git

projects[node_limit][version]= "1.x-dev"
;git clone --branch 6.x-1.x http://git.drupal.org/project/node_limit.git


projects[atrium_quota][download][type] = "git"
projects[atrium_quota][download][url] = "http://git.drupal.org/sandbox/laurentb/2220199.git"
projects[atrium_quota][type] = "module"


;******************************************************************************
;                            Additional theme
;******************************************************************************
projects[] = mobile_ginkgo
projects[] = atrium_simple

projects[planeo][download][type] = "git"
projects[planeo][download][url] = "http://git.drupal.org/sandbox/laurentb/2222019.git"
projects[planeo][type] = "theme"

;******************************************************************************
;                                     End
;******************************************************************************
