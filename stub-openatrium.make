;******************************************************************************
;                                Documentation
;******************************************************************************

; Description:
; A stub drush makefile to create an up-to-date OpenAtrium installation profile
; on a Pressflow core, or Drupal core.

; Instructions:
; In your platforms directory (/var/aegir/platforms), run:
;   "drush make openatrium-stub.make"
; Or create a new platform by calling this makefile using Aegir's web interface
; ('Makefile' field on node/add/platform):
;   "/var/aegir/makefiles/planeo_makefiles/stub-openatrium.make"
; Better yet, point to the raw (blob_plain) display of latest git commit
; remotely, so you can keep an exact reference in Aegir:
;   e.g., http://drupalcode.org/project/planeo_makefiles.git/blob_plain/<most recent commit hash>:/stub-openatrium.make

;******************************************************************************
;                                   General
;******************************************************************************

; drush make API version
api = 2

; Drupal core
core = 6.x
;projects[drupal][version] = 6.30

;******************************************************************************
;                               Include files
;******************************************************************************

; get fast core and set version
includes[pressflow] = "core-pressflow.make"
;projects[pressflow][download][branch] = "master"
projects[pressflow][download][tag] = "pressflow-6.28.111"

; OR get standard Drupal and set version
;includes[drupal] = "core-drupal.make"
;projects[drupal][download][tag] = "6.27"

includes[openatrium] = "profile-openatrium.make"
;projects[openatrium][download][branch] = "master"
projects[openatrium][download][tag] = "6.x-1.7"


;******************************************************************************
;                                  Patches
;******************************************************************************





;******************************************************************************
;                                     End
;******************************************************************************
